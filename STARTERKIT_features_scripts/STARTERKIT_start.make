; $Id$

; Drupal version.
core = "6.x"
api = "2"

; Drupal core.
projects[] = "drupal"

; Basic feature.
projects[STARTERKIT_basic][type] = "module"
projects[STARTERKIT_basic][subdir] = "features"
projects[STARTERKIT_basic][download][type] = "svn"
projects[STARTERKIT_basic][download][url] = "svn://svn.example.com/features/STARTERKIT_basic/"
