
DESCRIPTION
-----------

Features Scripts are command line shell scripts designed to help with building Drupal
sites using Features, Drush and Drush Make. It actually automates the processes of
installing new Drupal sites and adding new features to them, both used in site building.

OVERVIEW OF THE CONCEPT
-----------------------

These scripts assume the following:

- Your site is been build fully with the help of features. Therefore there is a need
  to separated everything what is related to a particular functionality into a particular
  feature module.

- A feature is bundling not only it's exportable components, but also a make file
  for downloading its dependencies, and a shell script for making all the other
  changes related to a feature where needed.

- Your starting point, all the functionality which you need for using this workflow
  (all what is on top of a naked Drupal install in this case) is bundled in a feature
  which we call the basic feature. The script contains only how to download and
  install Drupal and this basic feature.

USAGE
-----

Scripts are designed to be executed from the root path of a Drupal site or independent
of any Drupal sites entirely.

EXAMPLES

- Build and install a Drupal site start point.
  my_start example example.com

- Build and enable a desired feature from your existing features repository.
  my_feature examplefeature

REQUIREMENTS
------------

- Drush - latest HEAD

- Drush Make - latest stable

WINDOWS USERS

- basic POSIX commands from the msysGit package
  http://code.google.com/p/msysgit/

- Drush on Windows
  http://drupal.org/node/594744

- If you are experiencing problems related to Drush on Windows take a look at issue #766080.
  http://drupal.org/node/766080
